#use wml::debian::translation-check translation="21882a152ab072f844cf526dc172ff70559e05e7"
<define-tag pagetitle>Debian 9 actualizado: publicada la versión 9.10</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la décima actualización de su
distribución «antigua estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «antigua estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction base-files "Actualizado para esta versión; añade VERSION_CODENAME a os-release">
<correction basez "Decodifica correctamente cadenas codificadas en base64url">
<correction biomaj-watcher "Corrige actualizaciones de jessie a stretch">
<correction c-icap-modules "Añade soporte para clamav 0.101.1">
<correction chaosreader "Añade dependencia con libnet-dns-perl, que faltaba">
<correction clamav "Nueva versión «estable» del proyecto original: añade límite de tiempo de escaneo para mitigar los efectos de bombas zip [CVE-2019-12625]; corrige escritura fuera de límites en la biblioteca bzip2 de NSIS [CVE-2019-12900]">
<correction corekeeper "No usa un /var/crash con permisos universales de escritura con el script dumper; gestiona versiones anteriores del núcleo Linux de una manera más segura; no trunca los nombres de los volcados de memoria («cores») para ejecutables con espacios">
<correction cups "Corrige múltiples problemas de seguridad y de revelación de información: problemas de desbordamientos de memoria SNMP [CVE-2019-8696 CVE-2019-8675], de desbordamiento de memoria IPP, de denegación de servicio y de revelación de contenido de la memoria en el planificador">
<correction dansguardian "Añade soporte para clamav 0.101">
<correction dar "Recompilado para actualizar los paquetes <q>built-using</q>">
<correction debian-archive-keyring "Añade claves de buster; elimina claves de wheezy">
<correction fence-agents "Corrige problema de denegación de servicio [CVE-2019-10153]">
<correction fig2dev "No provoca fallo de segmentación en puntas de flecha circulares o semicirculares con una ampliación superior a 42 [CVE-2019-14275]">
<correction fribidi "Corrige salida de derecha a izquierda en modo texto del instalador de Debian">
<correction fusiondirectory "Comprobaciones más estrictas en búsquedas LDAP; añade dependencia con php-xml, que faltaba">
<correction gettext "xgettext() deja de provocar caídas cuando se ejecuta con la opción --its=FILE">
<correction glib2.0 "Crea directorio y fichero con permisos restrictivos cuando se usa el GKeyfileSettingsBackend [CVE-2019-13012]; evita lectura de memoria fuera de límites al formatear mensajes de error por UTF-8 inválido en GMarkup [CVE-2018-16429]; evita desreferencia NULL al analizar sintácticamente GMarkup inválido con una etiqueta de cierre mal construida, no emparejada con una etiqueta de apertura [CVE-2018-16429]">
<correction gocode "gocode-auto-complete-el: hace que «predependa» de auto-complete-el con número de versión para corregir actualizaciones de jessie a stretch">
<correction groonga "Mitiga elevación de privilegios mediante el cambio del propietario y grupo de los logs con la opción <q>su</q>">
<correction grub2 "Correcciones para el soporte de Xen UEFI">
<correction gsoap "Corrige problema de denegación de servicio si se compila una aplicación del servidor con el indicador -DWITH_COOKIES [CVE-2019-7659]; corrige problema con el receptor del protocolo DIME y cabeceras DIME mal construidas">
<correction gthumb "Corrige fallo de «doble liberación» [CVE-2018-18718]">
<correction havp "Añade soporte para clamav 0.101.1">
<correction icu "Corrige violación de acceso en la orden pkgdata">
<correction koji "Corrige problema de inyección de SQL [CVE-2018-1002161]; valida correctamente rutas SCM [CVE-2017-1002153]">
<correction lemonldap-ng "Corrige regresión de autenticación entre dominios; corrige vulnerabilidad de entidad externa XML">
<correction libcaca "Corrige problemas de desbordamiento de entero [CVE-2018-20545 CVE-2018-20546 CVE-2018-20547 CVE-2018-20548 CVE-2018-20549]">
<correction libclamunrar "Nueva versión «estable» del proyecto original">
<correction libconvert-units-perl "Recompilado sin modificaciones con número de versión corregido">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libebml "Aplica correcciones del proyecto original a lecturas fuera de límites de memoria dinámica («heap»)">
<correction libevent-rpc-perl "Corrige error de compilación debido a certificados SSL de prueba expirados">
<correction libgd2 "Corrige lectura no inicializada en gdImageCreateFromXbm [CVE-2019-11038]">
<correction libgovirt "Regenera certificados de prueba con fecha de expiración en el futuro lejano para evitar fallos de las pruebas">
<correction librecad "Corrige denegación de servicio mediante fichero manipulado [CVE-2018-19105]">
<correction libsdl2-image "Corrige múltiples problemas de seguridad">
<correction libthrift-java "Corrige elusión de la negociación SASL [CVE-2018-1320]">
<correction libtk-img "Deja de utilizar copias internas de los codecs JPEG, Zlib y PixarLog, con lo que corrige caídas">
<correction libu2f-host "Corrige fuga del contenido de la pila [CVE-2019-9578]">
<correction libxslt "Corrige elusión de la infraestructura de soporte de seguridad [CVE-2019-11068]; corrige lectura no inicializada del token xsl:number [CVE-2019-13117]; corrige lectura no inicializada con caracteres de agrupación («grouping chars») UTF-8 [CVE-2019-13118]">
<correction linux "Nueva versión del proyecto original con actualización de la ABI; correcciones de seguridad [CVE-2015-8553 CVE-2017-5967 CVE-2018-20509 CVE-2018-20510 CVE-2018-20836 CVE-2018-5995 CVE-2019-11487 CVE-2019-3882]">
<correction linux-latest "Actualizado para la ABI del núcleo 4.9.0-11">
<correction liquidsoap "Corrige compilación con Ocaml 4.02">
<correction llvm-toolchain-7 "Nuevo paquete para soportar la compilación de versiones nuevas de Firefox">
<correction mariadb-10.1 "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2805 CVE-2019-2627 CVE-2019-2614]">
<correction minissdpd "Impide una vulnerabilidad de «uso tras liberar» que permitiría que un atacante remoto provocara la caída del proceso [CVE-2019-12106]">
<correction miniupnpd "Corrige problemas de denegación de servicio [CVE-2019-12108 CVE-2019-12109 CVE-2019-12110]; corrige fuga de información [CVE-2019-12107]">
<correction mitmproxy "Pruebas de lista negra que requieren acceso a Internet; impide inserción de dependencias no deseadas con número de versión acotado superiormente">
<correction monkeysphere "Corrige error de compilación actualizando las pruebas para tener en cuenta que en stretch GnuPG ahora produce una salida diferente">
<correction nasm-mozilla "Nuevo paquete para soportar la compilación de versiones nuevas de Firefox">
<correction ncbi-tools6 "Empaquetado sin data/UniVec.* no libres">
<correction node-growl "Sanea la entrada antes de pasársela a exec">
<correction node-ws "Limita el tamaño de los datos a subir [CVE-2016-10542]">
<correction open-vm-tools "Corrige posible problema de seguridad con los permisos del directorio intermedio de preparación y con los de la ruta">
<correction openldap "Limita rootDN proxyauthz a sus propias bases de datos [CVE-2019-13057]; hace cumplir la sentencia ACL sasl_ssf en cada conexión [CVE-2019-13565]; corrige el fallo por el que slapo-rwm no liberaba el filtro original cuando el filtro reescrito era inválido">
<correction openssh "Corrige abrazo mortal en la determinación de coincidencias de claves">
<correction passwordsafe "No instala ficheros para traducción a otros idiomas en un subdirectorio extra">
<correction pound "Corrige «contrabando» de peticiones («request smuggling») mediante cabeceras modificadas [CVE-2016-10711]">
<correction prelink "Recompilado para actualizar los paquetes <q>built-using</q>">
<correction python-clamav "Añade soporte para clamav 0.101.1">
<correction reportbug "Actualiza nombres de versión, en línea con la publicación de buster">
<correction resiprocate "Resuelve un problema de instalación con libssl-dev e --install-recommends">
<correction sash "Recompilado para actualizar los paquetes <q>built-using</q>">
<correction sdl-image1.2 "Corrige desbordamientos de memoria [CVE-2018-3977 CVE-2019-5058 CVE-2019-5052] y acceso fuera de límites [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction signing-party "Corrige llamada insegura al intérprete de órdenes («shell») que habilita inyección de intérprete de órdenes a través de un identificativo de usuario («User ID») [CVE-2019-11627]">
<correction slurm-llnl "Corrige potencial desbordamiento de memoria dinámica («heap») en sistemas de 32 bits [CVE-2019-6438]">
<correction sox "Corrige varios problemas de seguridad [CVE-2019-8354 CVE-2019-8355 CVE-2019-8356 CVE-2019-8357 927906 CVE-2019-1010004 CVE-2017-18189 881121 CVE-2017-15642 882144 CVE-2017-15372 878808 CVE-2017-15371 878809 CVE-2017-15370 878810 CVE-2017-11359 CVE-2017-11358 CVE-2017-11332">
<correction systemd "No detiene el cliente ndisc en caso de error de configuración">
<correction t-digest "Recompilado sin modificaciones para evitar la reutilización de la versión 3.0-1 anterior a la época («pre-epoch»)">
<correction tenshi "Corrige problema con el fichero PID que permite que usuarios locales cancelen procesos arbitrarios [CVE-2017-11746]">
<correction tzdata "Nueva versión del proyecto original">
<correction unzip "Corrige análisis sintáctico incorrecto de valores de 64 bits en fileio.c; corrige problemas de bombas zip [CVE-2019-13232]">
<correction usbutils "Actualiza la lista de los ID USB">
<correction xymon "Corrige varios problemas de seguridad (exclusivos del servidor) [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubico-piv-tool "Corrige problemas de seguridad [CVE-2018-14779 CVE-2018-14780]">
<correction z3 "No da al SONAME de libz3java.so el valor libz3.so.4">
<correction zfs-auto-snapshot "Hace que los trabajos cron terminen sin mensajes de error tras el borrado de paquetes">
<correction zsh "Recompilado para actualizar los paquetes <q>built-using</q>">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «antigua estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2019 4435 libpng1.6>
<dsa 2019 4436 imagemagick>
<dsa 2019 4437 gst-plugins-base1.0>
<dsa 2019 4438 atftp>
<dsa 2019 4439 postgresql-9.6>
<dsa 2019 4440 bind9>
<dsa 2019 4441 symfony>
<dsa 2019 4442 cups-filters>
<dsa 2019 4442 ghostscript>
<dsa 2019 4443 samba>
<dsa 2019 4444 linux>
<dsa 2019 4445 drupal7>
<dsa 2019 4446 lemonldap-ng>
<dsa 2019 4447 intel-microcode>
<dsa 2019 4448 firefox-esr>
<dsa 2019 4449 ffmpeg>
<dsa 2019 4450 wpa>
<dsa 2019 4451 thunderbird>
<dsa 2019 4452 jackson-databind>
<dsa 2019 4453 openjdk-8>
<dsa 2019 4454 qemu>
<dsa 2019 4455 heimdal>
<dsa 2019 4456 exim4>
<dsa 2019 4457 evolution>
<dsa 2019 4458 cyrus-imapd>
<dsa 2019 4459 vlc>
<dsa 2019 4460 mediawiki>
<dsa 2019 4461 zookeeper>
<dsa 2019 4462 dbus>
<dsa 2019 4463 znc>
<dsa 2019 4464 thunderbird>
<dsa 2019 4465 linux>
<dsa 2019 4466 firefox-esr>
<dsa 2019 4467 vim>
<dsa 2019 4468 php-horde-form>
<dsa 2019 4469 libvirt>
<dsa 2019 4470 pdns>
<dsa 2019 4471 thunderbird>
<dsa 2019 4472 expat>
<dsa 2019 4473 rdesktop>
<dsa 2019 4475 openssl>
<dsa 2019 4475 openssl1.0>
<dsa 2019 4476 python-django>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4485 openjdk-8>
<dsa 2019 4487 neovim>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4492 postgresql-9.6>
<dsa 2019 4494 kconfig>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4506 qemu>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction pump "Sin desarrollo activo; problemas de seguridad">
<correction teeworlds "Problemas de seguridad; incompatible con servidores actuales">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «antigua estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «antigua estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Actualizaciones propuestas a la distribución «antigua estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Información sobre la distribución «antigua estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>
