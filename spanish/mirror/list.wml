#use wml::debian::template title="Réplicas de Debian (en todo el mundo)" BARETITLE=true
#use wml::debian::translation-check translation="b554c22abdbc4a6253951c9bf9610405d0c4f2cd" maintainer="Laura Arjona Reina"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#per-country">Réplicsa de Debian por país</a></li>
  <li><a href="#complete-list">Lista completa de réplicas</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian se distribuye (mediante <a href="https://www.debian.org/mirror/">réplicas</a>) a través de
cientos de servidores. Si planea <a href="../download">descargar</a> Debian, pruebe primero un servidor cercano.
Esto probablemente sea más rápido y también reduce la carga en nuestros
servidores centrales.</p>

<p class="centerblock">
  Existen réplicas de Debian en muchos países, y para algunas hemos agregado
  un alias <code>ftp.&lt;país&gt;.debian.org</code>.  Este alias usualmente
  apunta a una réplica que se sincroniza regularmente y rápidamente, y proporciona
  todas las arquitecturas de Debian. El repositorio de Debian siempre está disponible
  vía HTTP en la ubicación <code>/debian</code>
  del servidor.
</p>

<p class="centerblock">
  Otras réplicas pueden tener restricciones en qué replican
  (debido a limitaciones de espacio). Simplemente porque un sitio no es
  <code>ftp.&lt;país&gt;.debian.org</code> de un país no significa necesariamente
  que sea más lento o que esté menos actualizado que la réplica
  <code>ftp.&lt;país&gt;.debian.org</code>.
  De hecho, una réplica que contenga su arquitectura y esté más cerca al usuario,
  y por tanto sea más rápida, casi siempre es preferible frente a otras réplicas
  que estén más lejos.
</p>

<p>Para una descarga lo más rápida posible use la réplica más cercana
a usted, ya sea esta una réplica con alias de país o no.
Puede usar el
programa <a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> para determinar cuál es la réplica con menor
latencia; use un programa de descarga como
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> o bien
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a> para determinar el sitio que proporciona mejor
tasa de transferencia. Tenga en cuenta que la proximidad geográfica a 
menudo no es el factor más importante a la hora de determinar la máquina
que puede ofrecerle un mejor servicio.</p>

<p>
Si su sistema cambia mucho de ubicación, quizá le sirva bien una «réplica»
que esté respaldada por una red de distribución de contenidos 
(<abbr title="Content Delivery Network">CDN</abbr>) global.
El proyecto Debian mantiene 
<code>deb.debian.org</code> para este propósito y puede usarlo en su
archivo sources.list &mdash; consulte
<a href="http://deb.debian.org/">el sitio web del servicio</a> para más detalles.

<p><h2 id="per-country">Réplicas de Debian por país</h2>

<table border="0" class="center">
<tr>
  <th>País</th>
  <th>Sitio</th>
  <th>Arquitecturas</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 id="complete-list">Lista completa de réplicas</h2>

<table border="0" class="center">
<tr>
  <th>Nombre de la máquina</th>
  <th>HTTP</th>
  <th>Arquitecturas</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
