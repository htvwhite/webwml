#use wml::debian::template title="데비안 접근성"
#use wml::debian::translation-check translation="82f3d1721149ec88f5974028ed2c2d8d454b4071" maintainer="Sebul"
{#style#:<link rel="stylesheet" href="style.css" type="text/css" />:#style#}

<h2>프로젝트 설명</h2>

<p>데비안 접근성은 특히 장애인의 요구 사항에 적합한 운영 체제로 데비안을 개발하기 위한 내부 프로젝트입니다. 
데비안 접근성의 목표는 완전히 무료 소프트웨어를 기반으로 구축된 장애 있는 사용자에게 최대한의 독립성을 제공하는 완전히 액세스 가능한 시스템입니다.
</p>
<p>우리는 데비안 접근성이 
특정 사용자 그룹에 매우 특정한 문제를 수정하는 패치를 제공하거나 접근성 유효성 검사를 수행하고 결과에 따라 수정 제안을 제공함으로써 기존 패키지에 가치를 더할 것으로 예상합니다.
</p>

<h2><a id="email-list" name="email-list">이메일 목록</a></h2>

<p>데비안 접근성 메일링 리스트는 데비안 접근성의 중심 소통 지점입니다. 
특별한 도움이 필요한 데비안 시스템의 현재 사용자뿐만 아니라 잠재적 사용자를 위한 포럼 역할을 합니다. 
또한 접근성의 다양한 주제에 대한 개발 노력을 조정하는 데 사용됩니다.
구독하거나 취소하려면 
   <a href="https://lists.debian.org/debian-accessibility/">리스트 웹페이지</a>에서 할 수 있고, 
   <a href="https://lists.debian.org/debian-accessibility/">리스트 아카이브</a>를 읽을 수 있습니다.
</p>

<h2><a id="projects" name="projects">관련 소프트웨어</a></h2>

<p>소프트웨어를 카테고리로 분류하려는 첫 시도는 최선이 아닐 수 있습니다.
개선을 위한 제안을 <a href="#email-list">메일링 리스트</a>, 
또는 <a href="mailto:sthibault@debian.org">Samuel Thibault</a>에게 보내세요.
</p>
<ul>
  <li><a href="software#speech-synthesis">Speech synthesis 및 관련
      API</a></li>
  <li><a href="software#console">콘솔 (텍스트 모드) 스크린 리더</a></li>
  <li><a href="software#emacs">이맥스용 Screen review extension</a></li>
  <li><a href="software#gui">그래픽 유저 인터페이스</a>:
      <ul>
        <li><a href="software#gnome">GNOME 접근성</a></li>
        <li><a href="software#input">비표준 입력 방법</a></li>
      </ul></li>
</ul>

<h2><a id="goals" name="goals">프로젝트 목표</a></h2>

<ul>
  <li>접근성에 대한 정보와 문서를 제공합니다.</li>
  <li>필요한 경우 데비안 설치 프로세스를 포함하여 시스템 시작 단계에서 가능한 한 빨리 특수 주변장치용 드라이버와 같은 접근성 소프트웨어를 로드할 수 있는지 확인합니다. 
이것은 특별한 도움이 필요한 사람들이 자신의 시스템을 유지하는 동안 높은 수준의 독립성을 유지하기위한 것입니다.</li>
  <li>웹 사이트와 같은 데비안 핵심 인프라가 접근성 지침을 준수하는지 검증하고 보증합니다.</li>
  <li>비슷한 목표를 가진 서로 다른 프로젝트의 작성자를 모읍니다.</li>
  <li>업스트림 작성자가 제품을 데비안 용으로 패키지화하도록 돕습니다.</li>
  <li>상용 보조 기술 공급 업체에게 자유 소프트웨어 기반 시스템의 강점을 보여주고 소프트웨어를 리눅스로 이식하거나 오픈 소스로 전환하도록 고려합니다.
</li>
</ul>

<h2><a id="help" name="help">도우려면 무엇을 하나요?</a></h2>

<ul>
  <li>웹페이지를 개선하고 번역하기.</li>
  <li>로고 만들기.</li>
  <li>문서 및 번역.</li>
  <li>국제화 (단순 번역 이상, 아이디어를 위해
      <a href="software#i18nspeech">국제화된 음성 합성</a>을 보세요).</li>
  <li>debian.org 웹 사이트가 설정된 접근성 지침에 따라 액세스 가능한지 검증하고 찾은 결과에 따라 개선 제안을 제출합니다. 
언젠가는 Bobby Approval과 같은 것을 신청하는 것이 바람직할 수 있습니다.
</li>
</ul>

<h2><a id="links" name="links">링크</a></h2>

<ul>
  <li><a href="https://wiki.debian.org/accessibility">데비안 접근성 위키</a>.</li>
  <li><a href="https://wiki.debian.org/accessibility-maint">데비안 메인테이너용 데비안 접근성 위키</a>.</li>
  <li><a href="https://wiki.debian.org/accessibility-devel">데비안 접근성 개발자 위키</a>.</li>
  <li><a href="https://wiki.linuxfoundation.org/accessibility/start">자유 표준 그룹 접근성 작업 그룹</a>.</li>
  <li><a href="https://wiki.gnome.org/Accessibility">GNOME 접근성 프로젝트</a>.</li>
  <li><a href="https://community.kde.org/Accessibility">KDE 접근성 프로젝트</a>.</li>
  <li><a href="https://wiki.ubuntu.com/Accessibility">우분투 접근성</a></li>
  <li><a href="http://leb.net/blinux/">BLINUX</a>: 
시각 장애인을 위해 GNU/리눅스 운영체제의 유용성을 향상시킵니다.</li>
  <li><a href="http://www.linux-speakup.org/">리눅스 Speakup 프로젝트</a>.</li>
  <li><a href="http://www.w3.org/WAI/">W3C Web Accessibility Initiative
      (WAI)</a>:
      <ul>
        <li><a href="http://www.w3.org/TR/WCAG10/">Web Content Accessibility Guidelines</a> 
장애 있는 다양한 사람들을 위한 웹사이트에 액세스할 수 있게 하는 방법을 자세히 설명합니다.
</li>
        <li><a href="http://www.w3.org/TR/ATAG10/">Authoring Tool Accessibility Guidelines</a> 
다양한 도구를 만들 수 있는 웹 콘텐츠의 생산을 지원하는 방법을 설명합니다.
</li>
      </ul></li>
</ul>
