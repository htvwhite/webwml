#use wml::debian::blend title="About the Blend"
#use "navbar.inc"
#use wml::debian::translation-check translation="9bc4ecf1abce08efe4132833b3d0ba344ca3ff10" maintainer="galaxico"

<p>The <b>Debian Hamradio Pure Blend</b> is a project of the <a
href="https://wiki.debian.org/DebianHams/">Debian Hamradio Maintainers Team</a>
who collaborate on maintenance of amateur-radio related packages for Debian.
Every <a href="https://blends.debian.org/">Pure Blend</a> is a subset of Debian
that is configured to support a particular target group out-of-the-box. This
blend aims to support the needs of radio amateurs.</p>

<p>The Blend is built from a curated list of amateur radio software in Debian.
</p>

<h2>Metapackages</h2>

<p>Metapackages in Debian are packages that can be installed just like other
packages but that do not contain any software themselves, instead instructing
the packaging system to install a group of other packages.</p>

<p>See <a href="./get/metapackages">using the metapackages</a> for more
information on which metapackages are available and how to install the
metapackages on an existing Debian system.</p>
