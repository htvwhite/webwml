#use wml::debian::template title="debian.community domain"
#use wml::debian::faqs
#use wml::debian::translation-check translation="92233062b10a767a6a4fc85a6e524d91dbe64868" maintainer="Erik Pfannenstein"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p style="display: block; border: solid; padding: 1em; background-color: #FFF29F;">
 <i class="fa fa-unlink fa-3x"></i>
 Falls Sie einem <a href="https://debian.community">debian.community</a>-Link auf diese 
 Seite gefolgt sind: Die Seite, die Sie sehen wollen, existiert nicht mehr. Lesen Sie 
 hier, warum.
</p>

<ul class="toc">
 <li><a href="#background">Hintergrund</a></li>
 <li><a href="#statements">Vorherige Stellungnahmen</a></li>
 <li><a href="#faq">Häufig gestellte Fragen</a></li>
</ul>

<h2><a id="background">Hintergrund</a></h2>

<p>
 Im Juli 2022 hat die World Intellectual Property Organization (Weltorganisation 
 für geistiges Eigentum) festgestellt, dass die Domain 
 <a href="https://debian.community/">debian.community</a> in böser Absicht 
 registriert und dazu verwendet wurde, die Handelsmarken des Debian-Projekts zu 
 beschädigen. Es ordnete an, dass die Domain an das Projekt übergeben wird.
</p>

<h2><a id="statements">Vorherige Stellunghnahmen</a></h2>

<h3><a id="statement-debian">
 Debian-Projekt: Belästigung durch Daniel Pocock
</a></h3>

<p>
 <i>Erstveröffentlichung: <a href="$(HOME)/News/2021/20211117">17. November 2021</a></i>
</p>


<p>
Dem Debian-Projekt sind mehrere Beiträge (Posts) bekannt, die ein gewisser 
Daniel Pocock auf mehreren Websites veröffentlicht hat. Daniel Pocock 
stellt sich dabei als Debian-Entwickler dar.
</p>

<p>
Hr. Pocock hat keine Verbindungen zu Debian. Er ist weder ein 
Debian-Entwickler noch überhaupt ein Mitglieder der Debian-Gemeinschaft. 
Er gehörte früher zu den Debian-Entwicklern, wurde aber wegen seines 
Verhaltens, das Debians Ansehen und der Gemeinschaft an sich geschadet hat, 
ausgeschlossen. Seit 2018 ist er daher kein Mitglied des Debian-Projekts 
mehr. Darüber hinaus wurde ihm die Teilnahme an der Debian-Gemeinschaft in 
jedweder Form, einschließlich technischer Beiträge sowie die Teilnahme an 
Online-Versammlungen, Konferenzen und/oder anderen Veranstaltungen, 
untersagt. Er hat somit kein Recht, Debian auf irgendeine Art zu vertreten 
oder sich als Debian-Entwickler bzw. als Mitglied der Debian-Gemeinschaft 
auszugeben. 
</p>


<p>
In der Zeit seit seinem Ausschluss hat sich Hr. Pocock einer andauernden 
und immensen Rachekampagne verschrieben, im Zuge derer er eine Vielzahl von 
aufhetzerischen Online-Posts, vor allem auf einer angeblichen Debian-Website, 
veröffentlichte. In diesen Posts geht es nicht nur um Debian, sondern auch 
um mehrere Entwickler und Freiwillige. Er präsentiert sich dabei in seiner 
Kommunikation und in seiner Öffentlichkeitsarbeit weiterhin als Mitglied der 
Debian-Gemeinschaft. Eine Auflistung der Websiten, die wirklich zu den 
offiziellen Kommunikationskanälen gehören, finden Sie in diesem Artikel. Wir 
erwägen, ihn u. a. wegen Verleumdung, übler Nachrede und Belästigung 
juristisch zur Rechenschaft zu ziehen. </p>

<p>
Debian stellt sich als Gemeinschaft gegen Belästigung. Wir verfügen über einen 
Verhaltenskodex, der uns in unserer Reaktion auf schädliches Verhalten 
anleitet, und wir werden auch zukünftig unsere Gemeinschaft und ihre Mitglieder 
beschützen. Bitte zögern Sie nicht, sich ans Community-Team zu wenden, wenn Sie 
Bedenken haben oder Unterstützung benötigen. Währenddessen sind sämtliche 
Rechte Debians und seiner Freiwilligen vorbehalten. 
</p>

<h3><a id="statement-other">Stellungnahmen von anderen Projekten</a></h3>

<ul>
 <li>
  <a href="https://fsfe.org/about/legal/minutes/minutes-2019-10-12.en.pdf#page=17">
   Free Software Foundation Europe e.V.</a> (Erstveröffentlichung 12. Oktober 2019)
 </li>
 <li>
  <a href="https://openlabs.cc/en/statement-we-have-been-a-target-of-disinformation-efforts-our-initial-reaction/">
   Open Labs</a> (Erstveröffentlichung 26. Mai 2021)
 </li>
 <li>
  <a href="https://communityblog.fedoraproject.org/statement-on-we-make-fedora/">
   Fedora</a> (Erstveröffentlichung 31. Jänner 2022)
 </li>
</ul>

<h2><a id="faq">Häufig gestellte Fragen</a></h2>

<question>
 Warum wurde die Domain an Debian übertragen?
</question>

<answer><p>
 Debian hat beim WIPO eine Beschwerde eingereicht, dass die Domain für 
 schädliches Treiben mit Debians Handelsmarken verwendet werde. Im Juli 2022 
 stimmte das WIPO-Gremium zu, dass der vorherige Halter weder Rechte noch 
 Interesse an der Marke hatte, sondern sie nur in böser Absicht verwendete. 
 Daher hat die WIPO die Domain dem Debian-Projekt zugestanden.
</p></answer>

<question>
 Warum war Debian mit dem Inhalt nicht einverstanden?
</question>

<answer><p>
 Der Inhalt der Website <a href="https://debian.community/">debian.community</a>
 beschädigte die Handelsmarken von Debian, indem er sie mit gegenstandslosen 
 Vorhaltungen und Links auf Kulte sowie Andeutungen, dass Freiwillige versklavt 
 und missbraucht würden, in Verbindung brachte.
</p></answer>

<question>
 Wer stand hinter der vorherigen Website?
</question>

<answer><p>
 Der vorherige Halter der Domain war die Free Software Contributors Association, 
 ein unregistrierter Zusammenschluss aus der Schweiz. In seiner Beschwerde an die 
 WIPO vermutete Debian, dass dieser Zusammenschluss ein Alter Ego für Daniel 
 Pocock sein könnte.
</p></answer>

<question>
 Kann ich den Beschluss der WIPO irgendwo im Volltext lesen?
</question>

<answer><p>
 Ja, er ist 
 <a href="https://www.wipo.int/amc/en/domains/decisions/pdf/2022/d2022-1524.pdf">
 öffentlich archiviert, jedoch nur auf Englisch.</a>
</p></answer>

<question>
 Ist das Gremium der Meinung, dass die Artikel rufschädigend sind?
</question>

<answer><p>
 Dem Gremium ist es nicht gestattet, diesbezüglich eine Feststellung zu treffen – 
 es ist jenseits seiner Zuständigkeit. Es hat nur die Befugnis, festzustellen, ob 
 der Domain-Eigentümer die Domain in böser Absicht registriert hat, um 
 Debians Handelsmarken zu beschädigen.
</p></answer>

<question>
 Wird Debian weitere Schritte einleiten?
</question>

<answer><p>
 Diese Information kann nicht veröffentlicht werden. Das Debian-Projekt 
 beobachtet die Lage weiter und stimmt sich mit seinem Rechtsbeistand ab.
</p></answer>

<h2><a id="further-info">Weitere Informationen<a/></h2>

<ul>
 <li>
  <a href="$(HOME)/intro/people">
   Debian-Projekt: Leute: Wer wir sind und was wir machen</a>
 </li>
 <li>
  <a href="$(HOME)/intro/philosophy">
   Debian-Projekt: Unsere Philosophie: Wie wir es machen und warum</a>
 </li>
</ul>
