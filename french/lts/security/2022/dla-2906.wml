#use wml::debian::translation-check translation="aa941070981ae8395e41806838346e757fc1c16a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait deux vulnérabilités dans Django, un cadriciel populaire de
développement web basé sur Python :</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22818">CVE-2022-22818</a>

<p>Vulnérabilité XSS potentielle au moyen d'une étiquette de template
{% debug %}.</p>

<p>L'étiquette de template {% debug %} n'encodait pas correctement le
contexte en cours, constituant un vecteur d’attaque XSS.</p>

<p>Afin d'éviter cette vulnérabilité, {% debug %} ne fournit plus
d'information de sortie quand le réglage de DEBUG a la valeur False et
assure que toutes les variables de contexte sont correctement protégées
quand le réglage de DEBUG a la valeur True.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23833">CVE-2022-23833</a>

<p>Déni de service potentiel dans l'envoi de fichiers.</p>

<p>Passer certaines entrées à des formes multipart pourrait avoir pour
conséquence une boucle infinie lors de l'analyse des fichiers.</p>
</li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1:1.10.7-2+deb9u15.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2906.data"
# $Id: $
