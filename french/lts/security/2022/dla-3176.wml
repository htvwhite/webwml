#use wml::debian::translation-check translation="49c68e45aefc016c36a1a5441be764cf0a4d86aa" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans
clickhouse, un système de base de données orientée colonne.</p>


<p>Les vulnérabilités requièrent une authentification, mais peuvent être
déclenchées par tout utilisateur doté de droits en lecture. Cela signifie
que l'attaquant peut réaliser une reconnaissance sur la cible particulière
du serveur ClickHouse pour obtenir des identifiants valables. Tout jeu
d'identifiants devrait fonctionner, dans la mesure où même un utilisateur
avec les privilèges les plus bas peut déclencher toutes les vulnérabilités.
En déclenchant les vulnérabilités, un attaquant peut planter le serveur
ClickHouse, divulguer des contenus de mémoire ou même provoquer l'exécution
de code distant.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42387">CVE-2021-42387</a>:

<p>Une lecture de tas hors limites dans le codec de compression LZ4 de
Clickhouse lors de l'analyse d'une requête malveillante. Dans le cadre de
la boucle de LZ4::decompressImpl(), une valeur 16 bits non signée fournie
par l'utilisateur (<q>offset</q>) est lue à partir des données compressées.
L'offset est ensuite utilisé pour la longueur d'une opération de copie,
sans vérifier les limites supérieures de la source de l'opération de copie.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42388">CVE-2021-42388</a>:

<p>Une lecture de tas hors limites dans le codec de compression LZ4 de
Clickhouse lors de l'analyse d'une requête malveillante. Dans le cadre de
la boucle de LZ4::decompressImpl(), une valeur 16 bits non signée fournie
par l'utilisateur (<q>offset</q>) est lue à partir des données compressées.
L'offset est ensuite utilisé pour la longueur d'une opération de copie,
sans vérifier les limites inférieures de la source de l'opération de copie.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43304">CVE-2021-43304</a>:

<p>Un dépassement de tampon de tas dans le codec de compression LZ4 de
Clickhouse lors de l'analyse d'une requête malveillante. Il n'y pas de
vérification que les opérations de copie dans la boucle de
LZ4::decompressImpl, et particulièrement l'opération de copie arbitraire
wildCopy&lt;copy_amount&gt;(op, ip,copy_end), ne dépassent pas les limites
du tampon de destination.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43305">CVE-2021-43305</a>:

<p>Un dépassement de tampon de tas dans le codec de compression LZ4 de
Clickhouse lors de l'analyse d'une requête malveillante. Il n'y pas de
vérification que dans les opérations de copie dans la boucle de
LZ4::decompressImpl, et particulièrement l'opération de copie arbitraire
wildCopy&lt;copy_amount&gt;(op, ip,copy_end), ne dépassent pas les limites
du tampon de destination. Ce problème est très semblable au
<a href="https://security-tracker.debian.org/tracker/CVE-2021-43304">CVE-2021-43304</a>,
mais l'opération de copie vulnérable se trouve dans un appel différent de
wildCopy.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
18.16.1+ds-4+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets clickhouse.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de clickhouse, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/clickhouse">\
https://security-tracker.debian.org/tracker/clickhouse</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3176.data"
# $Id: $
