#use wml::debian::translation-check translation="2ef68d32b610cc4bd8b0c86f88bd49c89d52a51b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des erreurs d'analyse dans le module mp4 de Nginx, un serveur web et
mandataire inverse à hautes performances, pouvaient avoir pour conséquences un
déni de service, une divulgation de mémoire ou éventuellement l'exécution de
code arbitraire lors du traitement d'un fichier mp4 mal formé.</p>

<p>Ce module n'est activé que dans le paquet binaire nginx-extras..</p>

<p>De plus la vulnérabilité suivante a été corrigée.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3618">CVE-2021-3618</a>

<p>ALPACA est une attaque de confusion de contenu dans la couche du protocole
de l’application, exploitant un serveur TLS mettant en œuvre des protocoles
différents, mais utilisant des certificats compatibles, tels que les certificats
multidomaines ou jokers. Un attaquant, homme du milieu, ayant accès au trafic de
la victime sur la couche TCP/IP peut rediriger le trafic d’un sous-domaine à un
autre, aboutissant à une session TLS valable. Cela casse l’authentification TLS
et des attaques inter-protocoles peuvent être possibles là où le comportement
du protocole d’un service est compromis</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 1.14.2-2+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nginx.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nginx,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/nginx">\
https://security-tracker.debian.org/tracker/nginx</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3203.data"
# $Id: $
