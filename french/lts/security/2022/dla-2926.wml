#use wml::debian::translation-check translation="8c78ac5412450c0804e419817c53e83e91a0789b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>zsh, un langage de script et un interpréteur de commandes puissant,
n'empêchait pas l'expansion récursive de PROMPT_SUBST. Cela pourrait
permettre à un attaquant d'exécuter des commandes arbitraires dans un
interpréteur de commande d'utilisateur, par exemple en contraignant un
utilisateur de la fonction vcs_info à basculer vers une branche de Git avec
un nom contrefait pour l'occasion.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
5.3.1-4+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zsh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zsh, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zsh">\
https://security-tracker.debian.org/tracker/zsh</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2926.data"
# $Id: $
