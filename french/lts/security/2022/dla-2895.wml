#use wml::debian::translation-check translation="234d664902cd645f5d4d5d687f42bac6a46e746c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs erreurs de lecture hors limites ont été découvertes dans
qt4-x11. La plus grande menace d'après le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-3481">CVE-2021-3481</a>
(au moins) porte sur la disponibilité de données confidentielles de
l'application.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4:4.8.7+dfsg-11+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qt4-x11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qt4-x11, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qt4-x11">\
https://security-tracker.debian.org/tracker/qt4-x11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2895.data"
# $Id: $
