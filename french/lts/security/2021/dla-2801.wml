#use wml::debian::translation-check translation="295159cac7c2cac2c07772b8664c67767fc10c79" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été corrigés dans le démon cron.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9525">CVE-2017-9525</a>

<p>Correction du crontab de groupe contre une élévation de privilèges à
l’aide du script postinst.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9704">CVE-2019-9704</a>

<p>Une très grande crontab créée par un utilisateur pourrait planter le
démon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9705">CVE-2019-9705</a>

<p>Limitation du nombre de lignes de crontab à 10 000 pour empêcher un
utilisateur malveillant de créer une crontab excessivement grande.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9706">CVE-2019-9706</a>

<p>Correction d'un potentiel déni de service par une utilisation de mémoire
après libération.</p></li>

<li><p>En outre, un contournement de /etc/cron.{allow,deny} lors d'un échec
à l'ouverture a été corrigé. Si ces fichiers existent, alors il faut
qu'ils soient accessibles en lecture à l'utilisateur exécutant crontab(1).
S'ils ne le sont pas, les utilisateurs seront maintenant interdits par
défaut.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version .0pl1-128+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cron.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cron, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cron">\
https://security-tracker.debian.org/tracker/cron</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :\
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2801.data"
# $Id: $
