#use wml::debian::template title="Debian &ldquo;squeeze&rdquo; Installatie-informatie" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/squeeze/release.data"
#use wml::debian::translation-check translation="f36546f515e33fb0e590b3db17a516bf3d605f5f"

<h1>Debian installeren <current_release_squeeze></h1>


<p><strong>Debian 6.0 werd vervangen door
<a href="../../wheezy/">Debian 7.0 (<q>wheezy</q>)</a>. Sommige van de
installatie-images hieronder zijn mogelijk niet langer beschikbaar of werken
niet meer. Het wordt aanbevolen om in de plaats daarvan wheezy te installeren.
</strong></p>


<p>
<strong>Voor de installatie van Debian</strong> <current_release_squeeze>
(<em>squeeze</em>) kunt u een van de volgende images downloaden:
</p>

<div class="line">
<div class="item col50">
	<p><strong>netinst cd-image (meestal 135-175 MB)</strong></p>
		<netinst-images />
</div>

<div class="item col50 lastcol">
	<p><strong>visitekaart-cd-image (meestal 20-50 MB)</strong></p>
		<businesscard-images />
</div>

</div>

<div class="line">
<div class="item col50">
	<p><strong>volledige cd-sets</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>volledige dvd-sets</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>andere images (netboot, usb-stick, enz.)</strong></p>
<other-images />
</div>
</div>

<div id="firmware_nonfree" class="warning">
<p>
Indien een hardwareonderdeel van uw systeem <strong>>het laden van niet-vrije
firmware vereist</strong> voor het stuurprogramma van een apparaat, kunt u een
van de <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/squeeze/current/">\
tar-archieven met gebruikelijke firmwarepakketten</a>
gebruiken of een <strong>niet-officieel</strong> image downloaden met deze
<strong>niet-vrije</strong> firmware. Instructies over het gebruik van deze
tar-archieven en algemene informatie over het laden van firmware tijdens de
installatie is te vinden in de installatiehandleiding (zie onder Documentatie
hierna).
</p>
<div class="line">
<div class="item col50">
<p><strong>netinst (gewoonlijk 175-240 MB) <strong>niet-vrije</strong>
cd-images <strong>met firmware</strong></strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>Opmerkingen</strong>
</p>
<ul>
    <li>
	Voor het downloaden van volledige cd- of dvd-images wordt het gebruik van
	jigdo aanbevolen.
    </li><li>
	Voor de minder gebruikelijke architecturen is enkel een beperkt aantal
	images uit de cd- of dvd-set beschikbaar als ISO-bestand.
	De volledige sets zijn enkel via jigdo beschikbaar.
    </li><li>
	De multi-arch <em>cd</em>-images zijn respectievelijk bedoeld voor
    i386/amd64/powerpc en
	alpha/hppa/ia64; de installatie is vergelijkbaar met een installatie met
    een netinst-image voor één enkele architectuur.
    </li><li>
	Het multi-arch <em>dvd</em>-image is bedoeld voor i386/amd64; de
	installatie is vergelijkbaar met een installatie met een
	volledig cd-image voor één enkele architectuur. De dvd bevat ook
	al de broncode voor de opgenomen pakketten.
    </li><li>
	Voor de installatie-images zijn verificatiebestanden (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> en andere) te vinden in dezelfde map als de images.
    </li>
</ul>


<h1>Documentatie</h1>

<p>
<strong>Indien u slechts één document leest</strong> voor u met installeren
begint, lees dan onze
<a href="../i386/apa">Installatie-Howto</a> met een snel
overzicht van het installatieproces. Andere nuttige informatie is:
</p>

<ul>
<li><a href="../installmanual">Squeeze Installatiehandleiding</a><br />
met uitgebreide installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
en <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
met algemene vragen en antwoorden</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
met door de gemeenschap onderhouden documentatie</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Dit is een lijst met gekende problemen in het installatiesysteem van
Debian <current_release_squeeze>.  Indien u bij het installeren van Debian op
een probleem gestoten bent en dit probleem hier niet vermeld vindt, stuur ons
dan een
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">installatierapport</a>
waarin u de problemen beschrijft of
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">raadpleeg de wiki</a>
voor andere gekende problemen.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Errata voor release 6.0</h3>

<dl class="gloss">
	<dt>Op sommige sparc-systemen kan de installatie niet met cd gedaan worden</dt>
	<dd>Het Debian installatieprogramma voor Squeeze bevat geen PATA
    kernel drivers, waardoor het onmogelijk is om de installatie te voltooien
    vanaf cd-media op systemen die deze drivers vereisen om toegang te krijgen
    tot het cd-station (bijvoorbeeld Ultra 10), omdat het installatieprogramma
    er niet in zal slagen om het station te vinden. Het probleem kan omzeild
    worden door het installatieprogramma over het net op te starten en op die
    manier te vermijden dat toegang tot het cd-station nodig is tijdens de
    installatie.
	<br />
	Zie <a href="https://bugs.debian.org/610906">#610906</a>.<br />
	Dit zal opgelost worden in de volgende tussenrelease van Squeeze (6.0.1).</dd>

	<dt>Mogelijk niet-werkende gedetecteerde USB-brailleapparaten</dt>
	<dd>Wanneer men het Debian installatiesysteem een ingeplugd
    USB-brailleapparaat laat ontdekken, kan het zijn dat dit laatste enkel
    "scherm niet in tekstmodus" weergeeft. Dit is te wijten aan een mogelijke
    race tussen detectie en framebufferstart. Een tijdelijke oplossing is om
    <tt>brltty</tt> mee te geven aan de kernelcommandoregel om detectie af te
    dwingen.<br />
	Zie <a href="https://bugs.debian.org/611648">#611648</a>.
	</dd>

	<dt>Geen netwerkstuurprogramma for Sparc T2+</dt>
	<dd>het niu netwerkstuurprogramma, dat vereist is voor recentere T2+
    sparcsystemen is niet opgenomen in d-i (het Debian installatiesysteem),
    waardoor het onmogelijk is om er een installatiemethode op uit te voeren
    waarvoor een vroege instelling van het netwerk nodig is. Het stuurprogramma
    is opgenomen in kernelpakketten, zodat verwacht mag worden dat het netwerk
    normaal zal functioneren na het voltooien van de installatie.<br />
	Zie <a href="https://bugs.debian.org/608516">#608516</a>.
	</dd>

	<dt>Op systemen met een aty-grafische kaart zal het installatiesysteem
    mogelijk niet correct opstarten</dt>
	<dd>
	Op systemen met een aty-grafische kaart (bijvoorbeeld Ultra 10) zal het
    installatiesysteem mogelijk niet correct opstarten, waarbij de kernel vroeg
    in de opstartfase bevriest met als laatste melding "console [tty0]
    ingeschakeld, bootconsole uitgeschakeld". Dit is een bug in de kernel
    waarvoor er een patch bestaat, hoewel het probleem mogelijk niet volledig
    opgelost is.<br />
	Dit probleem kan omzeild worden door een opstartparameter voor de kernel
    toe te voegen: 'video=atyfb:off'. Hierdoor wordt tijdens het opstarten de
    framebuffer uitgeschakeld, waardoor het mogelijk wordt het
    installatiesysteem (en de gewone kernel) op te starten op dergelijke
    systemen.<br />
	Zie <a href="https://bugs.debian.org/609466">#609466</a>.
	</dd>

	<dt>Voor sommige toetsenbordindelingen werkt de toetsenbordselectie niet
    met het grafische installatiesysteem</dt>
	<dd>
	Bij grafische installaties werkt het vooraf selecteren van het toetsenbord
    niet voor bepaalde combinaties (Bulgarije, Duitstalig Zwitserland, Zweden
    en Braziliaans). Ook wordt de gemaakte keuze niet gebruikt en gaat het
    systeem de standaardwaarde Amerikaans Engels (/etc/default/keyboard)
    gebruiken.<br />
	Zie <a href="https://bugs.debian.org/610843">#610843</a>.
	</dd>

	<dt>Mogelijke installatieproblemen met op RTL8169 gebaseerde
    netwerkkaarten</dt>
	<dd>Mogelijk is het Debian installatiesysteem niet in staat om op de
    RTL8169-familie gebaseerde netwerkkaarten te gebruiken tijdens de
    installatie, waaronder het downloaden van pakketten via deze kaarten
    tijdens de installatie. Het geïnstalleerde systeem heeft geen last van dit
    probleem.<br />
	Zie <a href="https://bugs.debian.org/558316">#558316</a> en gelijkaardige
    samengevoegde bugs.<br />
	Dit zal opgelost worden in de volgende tussenrelease van Squeeze (6.0.1).
	</dd>

	<dt>
	Start niet op na een succesvolle installatie op een btrfs-root</dt>
	<dd>De installatie eindigt zoals normaal, maar na het opnieuw opstarten
    resulteert dit in een initramfs busybox-prompt.<br />
	Zie <a href="https://bugs.debian.org/608538">#608538</a>.
	</dd>

	<dt>Windows wordt niet aan de grub-lijst toegevoegd</dt>
	<dd>
	Het Debian installatiesysteem detecteert Windows tijdens de installatie,
    maar voegt het niet toe aan het opstartmenu van grub. Voer bij wijze van
    voorlopige oplossing na de installatie het commando update-grub uit.<br />
	Zie <a href="https://bugs.debian.org/608025">#608025</a>.
	</dd>

	<dt>Maakt een partitietabel die niet compatibel is met Mac OS 9</dt>
	<dd>Er werd gesignaleerd dat het schijfindelingsgereedschap van het
    installatieprogramma een partitietabel aanmaakt welke door Mac OS 9 niet
    kan herkend worden, waardoor dit besturingssysteem niet langer kan
    opstarten. Hoewel de HFS+ partities compatibel zijn met Linux en Mac OS X,
    wordt aangeraden om alle voorzorgsmaatregelen te nemen bij een installatie
    op een machine met Mac OS 9.<br />
	Zie <a href="https://bugs.debian.org/604134">#604134</a>.
	</dd>

	<dt>Schijfindeling mislukt op kFreeBSD</dt>
	<dd>
	Er waren rapporten van falende schijfindelingen op kFreeBSD.
	Het probleem lijkt verband te houden met partitieafbakening/uitgebreide
    partities.<br />
	Zie <a href="https://bugs.debian.org/593733">#593733</a>,
	<a href="https://bugs.debian.org/597088">#597088</a> en
	<a href="https://bugs.debian.org/602129">#602129</a>.
	</dd>

	<dt>Netwerk/grafische/opslagkaart werkt niet behoorlijk</dt>
	<dd>
	Diverse hardware, met name netwerkkaarten, grafische kaarten en
    opslagbesturingsapparaten, vereisen binaire niet-vrije firmware om
    behoorlijk te functioneren.<br />
	Debian zet zich in voor de waarden van vrije software en zal het systeem
    nooit niet-vrije software laten vereisen (zie het
    <a href="https://www.debian.org/social_contract">Sociale Contract</a> van
    Debian). Daarom bevat het installatiesysteem geen niet-vrije firmware.<br />
	Maar indien u tijdens de installatie externe firmware wilt laden, staat het
    u vrij dit te doen. Het proces wordt gedocumenteerd in de
    installatiehandleiding.
	</dd>

	<dt>zipl-installatieprobleem dat installatie op s390 onmogelijk maakt</dt>
	<dd>
	Als er een speciale partitie gemaakt wordt voor de map /boot, dan mislukt
    na de installatie het opstarten van het systeem wanneer /boot eerder wordt
    aangekoppeld dan /.<br />
	Zie <a href="https://bugs.debian.org/519254">#519254</a>.
	</dd>

	<dt>Routers met bugs kunnen netwerkproblemen veroorzaken</dt>
	<dd>
	Indien u tijdens de installatie netwerkproblemen ervaart, kan dit
    veroorzaakt worden door een router ergens tussen u en de Debian
    spiegelserver welke "window scaling" niet correct afhandelt.
	Zie <a href="https://bugs.debian.org/401435">#401435</a> en dit
	<a href="http://kerneltrap.org/node/6723">kerneltrap artikel</a> voor
	details.<br />
	U kunt dit probleem omzeilen door "TCP window scaling" uit te schakelen.
    Activeer een shell en geef het volgende commando:<br />
	<tt>echo 0 &gt; /proc/sys/net/ipv4/tcp_window_scaling</tt><br />
	Voor het geïnstalleerde systeem zou u wellicht "TCP window scaling" niet
    volledig moeten uitschakelen. Met het volgende commando worden bereiken
    voor lezen en schrijven ingesteld die met bijna elke router zouden moeten
    werken:<br />
	<tt>echo 4096 65536 65536 &gt;/proc/sys/net/ipv4/tcp_rmem</tt><br />
	<tt>echo 4096 65536 65536 &gt;/proc/sys/net/ipv4/tcp_wmem</tt>
	</dd>

<!-- leaving this in for possible future use...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

	<dt>Partities groter dan 16 TiB worden niet ondersteund door ext4</dt>
	<dd>
	De hulpprogramma's voor het maken van ext4-bestandssystemen bieden geen
    ondersteuning voor het maken van bestandssystemen van meer dan 16 TiB.
	</dd>

	<dt>s390: niet-ondersteunde functies</dt>
	<dd>
	<ul>
		<li>momenteel is ondersteuning voor de DASD DIAG discipline niet
        beschikbaar</li>
	</ul>
	</dd>

  </dl>

<if-stable-release release="squeeze">
<p>
Voor de volgende release van Debian wordt gewerkt aan een verbeterde versie
van het installatiesysteem, die u ook kunt gebruiken om squeeze te installeren.
Raadpleeg voor de concrete informatie
<a href="$(HOME)/devel/debian-installer/">de pagina van het Debian-Installer project</a>.
</p>
</if-stable-release>
