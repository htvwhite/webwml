<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two security issues were discovered in pcs, a corosync and pacemaker
configuration tool:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1049">CVE-2022-1049</a>

    <p>It was discovered that expired accounts were still able to login
    via PAM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2735">CVE-2022-2735</a>

    <p>Ondrej Mular discovered that incorrect permissions on a Unix socket
    setup for internal communication could result in privilege escalation.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 0.10.8-1+deb11u1.</p>

<p>We recommend that you upgrade your pcs packages.</p>

<p>For the detailed security status of pcs please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pcs">\
https://security-tracker.debian.org/tracker/pcs</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5226.data"
# $Id: $
