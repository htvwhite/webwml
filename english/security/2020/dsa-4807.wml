<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>David Benjamin discovered a flaw in the GENERAL_NAME_cmp() function
which could cause a NULL dereference, resulting in denial of service.</p>

<p>Additional details can be found in the upstream advisory:
<a href="https://www.openssl.org/news/secadv/20201208.txt">https://www.openssl.org/news/secadv/20201208.txt</a></p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.1.1d-0+deb10u4.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4807.data"
# $Id: $
