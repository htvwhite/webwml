<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>This update ships updated CPU microcode for some types of Intel CPUs. In
particular it provides mitigations for the TAA (TSX Asynchronous Abort)
vulnerability. For affected CPUs, to fully mitigate the vulnerability it
is also necessary to update the Linux kernel packages as released in DSA
4564-1.</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 3.20191112.1~deb9u1.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 3.20191112.1~deb10u1.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>For the detailed security status of intel-microcode please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4565.data"
# $Id: $
