Starting results calculation at Sun Apr 17 00:00:19 2022

Option 1 "Felix Lechner"
Option 2 "Jonathan Carter"
Option 3 "Hideki Yamane"
Option 4 "None of the above"

In the following table, tally[row x][col y] represents the votes that
option x received over option y.

                  Option
              1     2     3     4 
            ===   ===   ===   === 
Option 1           52    84   151 
Option 2    294         266   327 
Option 3    229    69         290 
Option 4    188    22    53       



Looking at row 2, column 1, Jonathan Carter
received 294 votes over Felix Lechner

Looking at row 1, column 2, Felix Lechner
received 52 votes over Jonathan Carter.

Option 1 Reached quorum: 151 > 47.9765567751584
Option 2 Reached quorum: 327 > 47.9765567751584
Option 3 Reached quorum: 290 > 47.9765567751584


Dropping Option 1 because of Majority. (0.8031914893617021276595744680851063829787)  0.803 (151/188) <= 1
Option 2 passes Majority.              14.864 (327/22) > 1
Option 3 passes Majority.               5.472 (290/53) > 1


  Option 2 defeats Option 3 by ( 266 -   69) =  197 votes.
  Option 2 defeats Option 4 by ( 327 -   22) =  305 votes.
  Option 3 defeats Option 4 by ( 290 -   53) =  237 votes.


The Schwartz Set contains:
	 Option 2 "Jonathan Carter"



-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

The winners are:
	 Option 2 "Jonathan Carter"

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

The total numbers of votes tallied = 354
