<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in the cpio package.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14866">CVE-2019-14866</a>

     <p>It is possible for an attacker to create a file so when
     backed up with cpio can generate arbitrary files in the
     resulting tar archive. When the backup is restored the
     file is then created with arbitrary permissions.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.11+dfsg-4.1+deb8u2.</p>

<p>We recommend that you upgrade your cpio packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1981.data"
# $Id: $
