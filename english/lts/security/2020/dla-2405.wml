<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Oleg Kalnichevski discovered that httpcomponents-client, a Java library
for building HTTP-aware applications, can misinterpret a malformed
authority component in request URIs passed to the library as
java.net.URI object and pick the wrong target host for request
execution.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.5.2-2+deb9u1.</p>

<p>We recommend that you upgrade your httpcomponents-client packages.</p>

<p>For the detailed security status of httpcomponents-client please refer
to its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/httpcomponents-client">https://security-tracker.debian.org/tracker/httpcomponents-client</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2405.data"
# $Id: $
