<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been discovered in the PostgreSQL
database system, which could result in the execution of arbitrary
code or disclosure of memory content.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
9.6.22-0+deb9u1.</p>

<p>We recommend that you upgrade your postgresql-9.6 packages.</p>

<p>For the detailed security status of postgresql-9.6 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgresql-9.6">https://security-tracker.debian.org/tracker/postgresql-9.6</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2662.data"
# $Id: $
