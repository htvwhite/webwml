<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been discovered in imagemagick.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27751">CVE-2020-27751</a>

    <p>A flaw was found in MagickCore/quantum-export.c. An attacker who submits a
    crafted file that is processed by ImageMagick could trigger undefined behavior
    in the form of values outside the range of type
    `unsigned long long` as well as a shift exponent that is too large for
    64-bit type. This would most likely lead to an impact to application availability,
    but could potentially cause other problems related to undefined behavior.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20243">CVE-2021-20243</a>

    <p>A flaw was found in MagickCore/resize.c. An attacker who submits a crafted
    file that is processed by ImageMagick could trigger undefined behavior
    in the form of math division by zero.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20245">CVE-2021-20245</a>

    <p>A flaw was found in coders/webp.c. An attacker who submits a crafted file that
    is processed by ImageMagick could trigger undefined behavior in the form of
    math division by zero.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20309">CVE-2021-20309</a>

    <p>A division by zero in WaveImage() of MagickCore/visual-effects.c may trigger
    undefined behavior via a crafted image file submitted to an application using
    ImageMagick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20312">CVE-2021-20312</a>

    <p>An integer overflow in WriteTHUMBNAILImage of coders/thumbnail.c may trigger
    undefined behavior via a crafted image file that is submitted by an attacker
    and processed by an application using ImageMagick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20313">CVE-2021-20313</a>

    <p>A potential cipher leak when the calculate signatures in TransformSignature is possible.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
8:6.9.7.4+dfsg-11+deb9u13.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>For the detailed security status of imagemagick please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/imagemagick">https://security-tracker.debian.org/tracker/imagemagick</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2672.data"
# $Id: $
