<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in various parsers of
Blender, a 3D modeller/ renderer. Malformed .blend model files and
malformed multimedia files (AVI, BMP, HDR, CIN, IRIS, PNG, TIFF) may
result in the execution of arbitrary code.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.72.b+dfsg0-3+deb8u1.</p>

<p>We recommend that you upgrade your blender packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1465.data"
# $Id: $
