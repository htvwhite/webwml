#use wml::debian::translation-check translation="0f4f3e8210b7403850a5550da354ea08036f8c2f" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i webbrowseren chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21148">CVE-2021-21148</a>

    <p>Mattias Buelens opdagede et bufferoverløbsproblem i 
    JavaScript-biblioteket V8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21149">CVE-2021-21149</a>

    <p>Ryoya Tsukasaki opdagedet stakoverløbsproblem i implementeringen af Data 
    Transfer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21150">CVE-2021-21150</a>

    <p>Woojin Oh opdagede et problem med anvendelse efter frigivelse i 
    fildownloaderen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21151">CVE-2021-21151</a>

    <p>Khalil Zhani opdagede et problem med anvendelse efter frigivelse i 
    betalingssystemet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21152">CVE-2021-21152</a>

    <p>Et bufferoverløb blev opdaget i mediehåndteringen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21153">CVE-2021-21153</a>

    <p>Jan Ruge opdagede et stakoverløbsproblem i GPU-processen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21154">CVE-2021-21154</a>

    <p>Abdulrahman Alqabandi opdagede et bufferoverløbsproblem i 
    implementeringen af Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21155">CVE-2021-21155</a>

    <p>Khalil Zhani opdagede et bufferoverløbsproblem i implementeringen af Tab 
    Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21156">CVE-2021-21156</a>

    <p>Sergei Glazunov opdagede et bufferoverløbsproblem i 
    JavaScript-biblioteket V8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21157">CVE-2021-21157</a>

    <p>Et problem med anvendelse efter frigivelse blev opdaget i 
    implementeringen af Web Sockets.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 88.0.4324.182-1~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine chromium-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende chromium, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4858.data"
